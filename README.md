# Gitlab에 작업한 것 올리는 방법  

> 초기세팅 이후 명령어    

```sh
# 0. gitlab에 올라가있는 공유된 자료를 내 PC로 불러온다.  
$ git pull origin master  

# 1. 작업한 모든것을 stage에 올린다.  
$ git add .  

# 2. stage에 올라간 자료를 local repo에 올린다.  
$ git commit -m "무엇을 작업했는지에 대한 설명"

# 3. local repo에 있는 자료를 gitlab에 올려 자료를 공유한다.  
$ git push origin master
```


<img width="833" alt="2018-07-28 7 09 38" src="https://user-images.githubusercontent.com/25549306/43355588-d97e60ea-9299-11e8-9257-d76eb3a07a07.png">

## 요건사항  

1. 모바일용 배치 임의로 하는지 디자인이 나왔는지 (문의하기 버튼 클릭시 팝업인지, 링크 이동인지)  
2. 작업기간 약 2주 예상  
3. ie10이상 부터 구현   
4. 견적 150~